package main

import (
	"codeberg.org/momar/web"
	"codeberg.org/momar/web/middlewares"
	"fmt"
	"os"
)

func main() {
	mw := []web.Middleware{}
	if web.EnvFlag("ENABLE_INDEX", false) {
		mw = append(mw, middlewares.DirectoryIndex(middlewares.DirectoryIndexConfig{
			ShowDirectoriesFirst:      true,
			ShowHiddenFiles:           web.EnvFlag("ENABLE_HIDDEN", false),
			HideCachedCompressedFiles: os.Getenv("ENABLE_COMPRESSION") == "cached",
			UseUTCTimestamps:          true,
		}))
	}
	if err := web.Run(mw...); err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
}
