# web: just serve static files.

This is a Docker image that's built for exactly a single thing: serving static files. Use it for your
[JAMstack](https://jamstack.org/) applications, to serve your `.well-known` directory for Let's Encrypt, local
development, or basically anything else that just requires a simple file server. Its core is built in under 100
lines of Go, using the awesome [fasthttp](http://github.com/valyala/fasthttp) library for extreme performance.

To get started, just run the following command to serve the current working directory on
[http://localhost:8080](http://localhost:8080):

```bash
docker run --rm -it -p 8080:80 -v "$PWD:/var/www:ro" momar/web
```

The following options (each shown with their default value) are configurable through environment variables (`-e VARIABLE=value`):

- `NOT_FOUND=/404.html` - path to the 404 error page
- `NOT_FOUND_STATUS=404` - set a status code for the 404 error page (you can use "200" here for webapps based on the history API)
- `INDEX_FILES=index.html;index.htm` - semicolon-separated list of files that should be served when accessing a directory
- `ENABLE_INDEX=0` - show a directory index if no index file could be found
- `ENABLE_HIDDEN=0` - allow access to files and directories starting with a dot
- `ENABLE_COMPRESSION=1` - enable gzip/deflate/brotli compression on-the-fly; you can also use the special value `cached` to [create cached `.fasthttp.gz`
  files](https://godoc.org/github.com/valyala/fasthttp#FS), which is recommended when extending the image via a Dockerfile, and is not recommended when using volumes
- `ENABLE_RANGE=1` - enable [HTTP range requests](https://developer.mozilla.org/en-US/docs/Web/HTTP/Range_requests)
- `ENABLE_CORS=0` - enable [CORS](https://developer.mozilla.org/en/docs/Glossary/CORS) headers with `Origin: *`, `Headers: Range` and `Methods: GET, HEAD`
- `ENABLE_CACHE=1` - if set to 0, send a [`Cache-Control: no-cache`](https://developer.mozilla.org/de/docs/Web/HTTP/Headers/Cache-Control) header with each request
  (otherwise it will be set to `must-revalidate`)
- `ENABLE_DEBUG=0` - print all non-OK requests to standard output/`docker logs`
- `HOST=[::]` & `PORT=80` - listening address, doesn't normally have to be changed

I'm assuming that you're using Docker with a reverse proxy anyways if you want to use this, so this doesn't come with
HTTPS support - if you need that, go with [Caddy](https://caddyserver.com), or add a [Traefik 2](https://traefik.io)
to your setup.

This image is feature-complete and production-ready, and has been inspired by
[hastatic](https://github.com/abhin4v/hastatic) (which is unfortunately written in a really ugly programming language).

Also, doing a quick benchmark on a relatively bad computer with `siege -b -c 100 -t 60S` and access logs disabled on
the example `index.html` file, it's extremely fast:

| Server           | Compressed image size | Requests per second | Response time | Longest transaction | Memory consumption |
|:-----------------|----------------------:|--------------------:|--------------:|--------------------:|-------------------:|
| momar/web        |                  2 MB |             5196.34 |        0.02 s |              0.22 s |             ~11 KB |
| abhin4v/hastatic |                  5 MB |             2466.33 |        0.04 s |              0.25 s |            ~220 MB |
| nginx:alpine     |                  9 MB |             5272.68 |        0.02 s |              0.32 s |             ~14 KB |
| httpd:alpine     |                 32 MB |             4555.74 |        0.02 s |              0.30 s |             ~30 KB |
| caddy:alpine     |                 14 MB |             4400.65 |        0.02 s |              0.28 s |            ~ 40 KB |
