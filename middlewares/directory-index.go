package middlewares

import (
	"fmt"
	"html"
	"net/url"
	"os"
	"sort"
	"strings"

	"codeberg.org/momar/web"
	"github.com/valyala/fasthttp"
)

const directoryIndexHeader = `<!doctype html>
<html style="background: #fff">
<head>
    <title>%s</title>
    <style>
        #index { margin: 0; padding: 1em; display: inline-flex; flex-direction: column; }
        #index>li { padding: 0.5em; font-family: monospace; display: flex; }
        #index>li:hover { background: rgba(127,127,127,0.2); }
        #index>li>:first-child { margin-right: 1.75em; word-break: break-word; }
        #index>li>.meta { margin-left: auto; white-space: nowrap; max-width: 100%%; overflow: hidden; text-overflow: ellipsis; }
        @media screen and (max-width: 30em) {
            #index { display: flex;}
            #index>li { border-bottom: 1px solid rgba(127,127,127,0.4); flex-direction: column; }
            #index>li>:first-child { margin-right: 0; }
            #index>li:last-child { border-bottom: none; }
            .meta { font-size: 0.85em; }
        }
        /* Uses Teenyicons (Copyright (c) 2020, Anja van Staden): https://github.com/teenyicons/teenyicons */
        #path { padding-left: 1.75em; background: url('data:image/svg+xml,<svg viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg" width="15" height="15"><path d="M9.146 6.854l.354.353.707-.707-.353-.354-.708.708zM7.5 4.5l.354-.354-.354-.353-.354.353.354.354zM5.146 6.146l-.353.354.707.707.354-.353-.708-.708zM14.5 7.5H14h.5zm-7 7V14v.5zm0-14V0v.5zm-7 7H0h.5zm9.354-1.354l-2-2-.708.708 2 2 .708-.708zm-2.708-2l-2 2 .708.708 2-2-.708-.708zM7 4.5V11h1V4.5H7zm7 3A6.5 6.5 0 017.5 14v1A7.5 7.5 0 0015 7.5h-1zM7.5 1A6.5 6.5 0 0114 7.5h1A7.5 7.5 0 007.5 0v1zM1 7.5A6.5 6.5 0 017.5 1V0A7.5 7.5 0 000 7.5h1zm-1 0A7.5 7.5 0 007.5 15v-1A6.5 6.5 0 011 7.5H0z" fill="currentColor"></path></svg>') no-repeat 0 center; }
        .file { padding-left: 1.75em; background: url('data:image/svg+xml,<svg viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg" width="15" height="15"><path d="M4.5 6.995H4v1h.5v-1zm6 1h.5v-1h-.5v1zm-6 1.998H4v1h.5v-1zm6 1.007h.5v-1h-.5v1zm-6-7.003H4v1h.5v-1zM8.5 5H9V4h-.5v1zm2-4.5l.354-.354L10.707 0H10.5v.5zm3 3h.5v-.207l-.146-.147-.354.354zm-9 4.495h6v-1h-6v1zm0 2.998l6 .007v-1l-6-.007v1zm0-5.996L8.5 5V4l-4-.003v1zm8 9.003h-10v1h10v-1zM2 13.5v-12H1v12h1zM2.5 1h8V0h-8v1zM13 3.5v10h1v-10h-1zM10.146.854l3 3 .708-.708-3-3-.708.708zM2.5 14a.5.5 0 01-.5-.5H1A1.5 1.5 0 002.5 15v-1zm10 1a1.5 1.5 0 001.5-1.5h-1a.5.5 0 01-.5.5v1zM2 1.5a.5.5 0 01.5-.5V0A1.5 1.5 0 001 1.5h1z" fill="currentColor"></path></svg>') no-repeat 0 center; }
        .dir { font-weight: bold; padding-left: 1.75em; background: url('data:image/svg+xml,<svg viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg" width="15" height="15"><path d="M.5 12.5v-10a1 1 0 011-1h4l2 2h6a1 1 0 011 1v8a1 1 0 01-1 1h-12a1 1 0 01-1-1z" stroke="currentColor"></path></svg>') no-repeat 0 center; }
        .empty { padding-left: 1.75em; opacity: 0.6; }
    </style>
</head>
<body>
    <ul id="index">
        <li>
            <strong id="path">%s</strong>
            <script type="text/javascript">
                document.getElementById("path").innerHTML =
                    (window.location.host + window.location.pathname).replace(/\/$/, "").split("/").map(
                        (part, i, all) => "<a href=\"" + window.location.protocol + "//" + all.slice(0, i+1).join("/") + "/\">" + decodeURIComponent(part).replace(/&/g, "&amp;").replace(/</g, "&lt;") + "</a> / "
                    ).join("");
                window.title = window.location.pathname.replace(/\/$/, "") + "/";
            </script>
        </li>
`
const directoryIndexEmpty = `        <li><span class="empty">empty directory</span></li>
`
const directoryIndexEntry = `        <li><a href="%s" class="%s">%s</a> <span class="meta">%.2f %s; %s</span></li>
`
const directoryIndexFooter = `    </ul>
</body>
</html>
`

type directoryIndexSortFileinfos struct {
	directoriesFirst bool
	fileinfos        []os.FileInfo
}

func (a directoryIndexSortFileinfos) Len() int {
	return len(a.fileinfos)
}
func (a directoryIndexSortFileinfos) Less(i, j int) bool {
	if a.directoriesFirst {
		if a.fileinfos[i].IsDir() && !a.fileinfos[j].IsDir() {
			return true
		}
		if !a.fileinfos[i].IsDir() && a.fileinfos[j].IsDir() {
			return false
		}
	}
	return a.fileinfos[i].Name() < a.fileinfos[j].Name()
}
func (a directoryIndexSortFileinfos) Swap(i, j int) {
	a.fileinfos[i], a.fileinfos[j] = a.fileinfos[j], a.fileinfos[i]
}

type DirectoryIndexConfig struct {
	ShowDirectoriesFirst      bool
	ShowHiddenFiles           bool
	HideCachedCompressedFiles bool
	UseUTCTimestamps          bool
}

func DirectoryIndex(cfg DirectoryIndexConfig) web.Middleware {
	return web.Middleware{
		web.OnHandled,
		func(ctx *fasthttp.RequestCtx) bool {
			if ctx.Response.StatusCode() == 403 && string(ctx.Response.Body()) == "Directory index is forbidden" {
				println(strings.TrimSuffix(string(ctx.Path()), "/"))
				f, err := os.Open("./" + strings.TrimPrefix(strings.TrimSuffix(string(ctx.Path()), "/"), "/"))
				if err != nil {
					ctx.Logger().Printf("couldn't open directory: %s", err)
					return true
				}
				fileinfos, err := f.Readdir(0)
				f.Close()
				if err != nil {
					ctx.Logger().Printf("couldn't read directory: %s", err)
					return true
				}

				ctx.Response.Reset()
				ctx.Response.SetStatusCode(200)
				ctx.Response.Header.Set("Content-Type", "text/html; charset=utf-8")

				basePathEscaped := html.EscapeString(string(ctx.Request.URI().Path()))
				basePathParts := strings.Split(strings.TrimSuffix(string(ctx.Request.URI().Path()), "/"), "/")[1:]
				basePathFormatted := "<a href=\"" + Up(len(basePathParts)) + "\">/</a>"
				for i, part := range basePathParts {
					basePathFormatted += " / <a href=\"" + Up(len(basePathParts)-1-i) + "\">" + html.EscapeString(part) + "</a>"
				}
				if len(basePathParts) > 0 {
					basePathFormatted += " /"
				}
				ctx.WriteString(fmt.Sprintf(directoryIndexHeader, basePathEscaped, basePathFormatted))

				if len(fileinfos) == 0 {
					ctx.WriteString(directoryIndexEmpty)
				} else {
					sort.Sort(directoryIndexSortFileinfos{cfg.ShowDirectoriesFirst, fileinfos})
					for _, fileinfo := range fileinfos {
						if !cfg.HideCachedCompressedFiles {
							for _, suffix := range fasthttp.FSCompressedFileSuffixes {
								if strings.HasSuffix(fileinfo.Name(), suffix) {
									continue
								}
							}
						}

						if !cfg.ShowHiddenFiles && strings.HasPrefix(fileinfo.Name(), ".") {
							continue
						}

						fileUrl := url.PathEscape(fileinfo.Name())
						fileType := "file"
						if fileinfo.IsDir() {
							fileType = "dir"
							fileUrl += "/"
						}

						fileName := html.EscapeString(fileinfo.Name())

						fileSize := float64(fileinfo.Size()) / 1024
						fileSizeSuffix := "KiB"
						if fileSize > 1024 {
							fileSize = fileSize / 1024
							fileSizeSuffix = "MiB"
						}
						if fileSize > 1024 {
							fileSize = fileSize / 1024
							fileSizeSuffix = "GiB"
						}

						fileTimestamp := fileinfo.ModTime()
						if cfg.UseUTCTimestamps {
							fileTimestamp = fileTimestamp.UTC()
						}
						fileDate := fileTimestamp.Format("2006-01-02 15:04:05 MST")

						ctx.WriteString(fmt.Sprintf(directoryIndexEntry, fileUrl, fileType, fileName, fileSize, fileSizeSuffix, fileDate))
					}
				}
				ctx.WriteString(directoryIndexFooter)
			}
			return true
		},
	}
}

func Up(n int) string {
	if n == 0 {
		return "./"
	}
	r := ""
	for i := 0; i < n; i++ {
		r += "../"
	}
	return r
}
