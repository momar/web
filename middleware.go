package web

import (
	"github.com/valyala/fasthttp"
)

type Middleware struct {
	Position MiddlewarePosition
	Run      func(ctx *fasthttp.RequestCtx) bool
}

type MiddlewareResult bool

const (
	DoStop     MiddlewareResult = false
	DoContinue MiddlewareResult = true
)

type MiddlewarePosition uint8

const (
	OnBegin MiddlewarePosition = iota
	OnHandled
	OnEnd
)

func RunMiddlewares(middlewares []Middleware, ctx *fasthttp.RequestCtx, pos MiddlewarePosition) bool {
	for _, middleware := range middlewares {
		if middleware.Position == pos {
			if !middleware.Run(ctx) {
				return false
			}
		}
	}
	return true
}
