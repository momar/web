module codeberg.org/momar/web

go 1.14

require (
	github.com/andybalholm/brotli v1.0.4 // indirect
	github.com/klauspost/compress v1.13.6 // indirect
	github.com/valyala/fasthttp v1.31.0
)
