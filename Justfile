build:
	CGO_ENABLED=0 go build -a -ldflags '-s -w -extldflags "-static"' -o web.x64 ./cmd
	upx web.x64
	CGO_ENABLED=0 GOARCH=arm64 go build -a -ldflags '-s -w -extldflags "-static"' -o web.arm64 ./cmd
	upx web.arm64
